﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atsd_lab_6
{
    internal class Program
    {
        private static void HeapSort(float[] array)
        {
            int n = array.Length;

            for (int i = n / 2 - 1; i >= 0; i--)
            {
                Heapify(array, n, i);
            }

            for (int i = n - 1; i >= 0; i--)
            {
                float temp = array[0];
                array[0] = array[i];
                array[i] = temp;

                Heapify(array, i, 0);
            }
        }
        private static void Heapify(float[] array, int n, int i)
        {
            int largest = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;

            if (left < n && array[left] > array[largest])
                largest = left;

            if (right < n && array[right] > array[largest])
                largest = right;

            if (largest != i)
            {
                float temp = array[i];
                array[i] = array[largest];
                array[largest] = temp;

                Heapify(array, n, largest);
            }
        }

        private static void ShellSort(float[] arrayShell)
        {
            int increment = 3;
            while (increment > 0)
            {
                for (int i = 0; i < arrayShell.Length; i++)
                {
                    int j = i;
                    float temp = arrayShell[i];
                    while ((j >= increment) && (arrayShell[j - increment] > temp))
                    {
                        arrayShell[j] = arrayShell[j - increment];
                        j -= increment;
                    }
                    arrayShell[j] = temp;
                }
                if (increment / 2 != 0)
                    increment /= 2;
                else if (increment == 1)
                    increment = 0;
                else
                    increment = 1;
            }
        }

        private static void CountingSort(char[] array)
        {
            char max = array.Max();
            char min = array.Min();

            char[] counts = new char[max - min + 1];
            char[] output = new char[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                counts[array[i] - min]++;
            }
            for (int i = 1; i < counts.Length; i++)
            {
                counts[i] += counts[i - 1];
            }
            for (int i = array.Length - 1; i >= 0; i--)
            {
                output[counts[array[i] - min] - 1] = array[i];
                counts[array[i] - min]--;
            }

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = output[i];
            }
        }

        public static TimeSpan GetElapsedTimeOfSorting<T>(Action<T[]> sortFunc, Func<int, T> fill, int arraySize)
        {
            T[] array = new T[arraySize];
            for (int i = 0; i < arraySize; i++)
            {
                array[i] = fill.Invoke(i);
            }

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            sortFunc.Invoke(array);
            stopwatch.Stop();

            return stopwatch.Elapsed;
        }

        static void Main(string[] args)
        {
            Random random = new Random();

            int[] lengths = { 10, 100, 500, 1000, 2000, 5000, 10000 };
            foreach (var length in lengths)
            {
                Console.WriteLine($"HeapSort[{length}] Time: {GetElapsedTimeOfSorting(HeapSort, i => (float)(random.Next(10, 100) + random.NextDouble()), length).TotalSeconds} s.");
            }
            Console.WriteLine();
            foreach (var length in lengths)
            {
                Console.WriteLine($"ShellSort[{length}] Time: {GetElapsedTimeOfSorting(ShellSort, i => (float)(random.Next(-10, 250) + random.NextDouble()), length).TotalSeconds} s.");
            }
            Console.WriteLine();
            foreach (var length in lengths)
            {
                Console.WriteLine($"CountingSort[{length}] Time: {GetElapsedTimeOfSorting(CountingSort, i => (char)random.Next(-100, 10), length).TotalSeconds} s.");
            }

            Console.ReadKey();
        }
    }
}
